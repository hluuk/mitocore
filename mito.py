import cobra
import re

model = cobra.io.read_sbml_model('mitocore.xml')
model.optimize()

print("Model: %d reactions, %d metabolites, %d genes" % (len(model.reactions), len(model.metabolites), len(model.genes)))

def find(pattern, model, what):
	try:
		data = getattr(model, what)
		keys = filter(lambda x: not re.match(pattern, x) is None, data._dict.keys())
		result = [data[data._dict[key]] for key in keys]
	except:
		result = []
	return result

def mfind(pattern, model = model):
	'''
	  Find metabolite
	'''
	return find(pattern, model, "metabolites")

def rfind(pattern, model = model):
	'''
	  Find reaction
	'''
	return find(pattern, model, "reactions")

def olist(model = model):
	'''
	  Return all objective functions
	'''
	return rfind("OF")

def ofind(model = model):
	'''
	  Find objective function
	'''
	return [reaction for reaction in model.reactions if reaction.objective_coefficient > 0]

def ochange(pattern, model = model):
	'''
	  Change objective of the function of the model
	'''
	of = rfind("OF")
	myof = filter(lambda x: not re.search(pattern, x.name, re.IGNORECASE) is None, of)
	if len(myof) > 0:
			myof = myof[0]
			# set upper bound to very high in order not to limit
			# the optimal value
			myof.upper_bound = 1000.
			model.change_objective(myof.id)
			print("Changed objective function to [%s]: %s" % (myof.id, myof.name))

'''
  Return reactions where metabolite m participates
'''
def reactions(m):
	result = m.reactions
	print("%s" % '\n'.join(map(lambda x: x.name, result)))
	return list(result)

# reactions/metabolites of interest
roi = {
  "atp": mfind("atp"),
  "nadh": mfind("nadh"),
  "co2": mfind("co2"),
  "objective": rfind("OF")
}

def scraps():
	# cytosolic ATP
	atpc = model.metabolites.get_by_id("atp_c")
	# mitochondrial ATP
	atpm = model.metabolites.get_by_id("atp_m")

	for atp in [atpc, atpm]:
		print("metabolite: %s, compartment: %s, no. of reactions: %s, charge: %s, formula: %s "% (atp.name, atp.compartment, len(atp.reactions), atp.charge, atp.formula))

	len(atp.reactions)
